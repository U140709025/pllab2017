#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <limits.h>
#include <float.h>
using namespace std;

int main ( void ){
int arr[10];
bool b;
char c;
short s;
int i;
double r;
printf("max int %ld\n",INT_MAX);
printf("min int %ld\n",INT_MIN);
printf("memory adress %ld\n",&arr[0]);
printf("memory adress %ld\n",&arr[1]);
printf("%ld\n",arr);
printf("The size of array is:%ld\n",sizeof(arr));
printf("The size of bool is:%ld\n",sizeof(b));
printf("The size of short is:%ld\n",sizeof(s));
printf("The size of int is:%ld\n",sizeof(i));
printf("The size of char is:%ld\n",sizeof(c));
printf("The size of double is:%ld\n",sizeof(r));

return 0;
}

