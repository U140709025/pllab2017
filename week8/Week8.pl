use strict;
use warnings;

print "Enter your name: ";

my $name = <>;
chomp $name;

print "Enter your age: ";

my $age = <>;
chomp $age;

print "Hello, $name!  ";
print "On your next birthday, you will be ", $age+1, ".\n";

my $p = 10 / 7;

printf "I have written %f programs.\n", $p;
printf "I have written %6.2f programs.\n", $p;
printf "I have written %0.2f programs.\n", $p;

my @a = ("ali");

if (@a) {print "length of \@a = ". scalar @a . "\n"; }

else { print "\n@a is undefined or empty\n"; }

my $str = "Hello, World!";

$a = substr $str, 0, 8;

print "$a\n";

substr $str, 7, 5, "$name";
print "$str\n";


exit;

